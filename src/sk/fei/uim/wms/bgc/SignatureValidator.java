package sk.fei.uim.wms.bgc;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.codec.binary.Base64;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Pavol, Betka, Miso
 */
public class SignatureValidator {

    private static final String dsNamespace = "http://www.w3.org/2000/09/xmldsig#";                             // pdf zep 2.5 str 16
    private static final String xzepNamespace = "http://www.ditec.sk/ep/signature_formats/xades_zep/v1.0";      // -//-
    private static final String[] supportedCanonicalizationAlgorithms = {"http://www.w3.org/TR/2001/REC-xml-c14n-20010315"};   // pdf zep 2.5 str 24 
    private static final String[] supportedSingatureAlgorithms = {  // pdf zep 2.5 str 37
        "http://www.w3.org/2000/09/xmldsig#dsa-sha1",   
        "http://www.w3.org/2000/09/xmldsig#rsa-sha1",
        "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256",
        "http://www.w3.org/2001/04/xmldsig-more#rsa-sha384",
        "http://www.w3.org/2001/04/xmldsig-more#rsa-sha512",
        "http://www.w3.org/2000/09/xmldsig#sha1",
        "http://www.w3.org/2001/04/xmldsig#sha224",
        "http://www.w3.org/2001/04/xmlenc#sha256",
        "http://www.w3.org/2001/04/xmldsig-more#sha384",
        "http://www.w3.org/2001/04/xmlenc#sha512"
    };
    private static final String[] supportedTransformAlgorithms = { //pdf zep 2.5 str. 26
        "http://www.w3.org/TR/2001/REC-xml-c14n-20010315",
        "http://www.w3.org/2000/09/xmldsig#base64"
    };
    private static final String[] supportedDigestAlgorithms = { //pdf zep 2.5 str. 37
        "http://www.w3.org/2000/09/xmldsig#sha1",
        "http://www.w3.org/2001/04/xmldsig#sha224",
        "http://www.w3.org/2001/04/xmlenc#sha256",
        "http://www.w3.org/2001/04/xmldsig-more#sha384",
        "http://www.w3.org/2001/04/xmlenc#sha512"
    };

    private File xmlFile;
    private String xml;
    private Document doc;
    private final StringBuilder report;

    // constructor only initialize report
    public SignatureValidator() {
        report = new StringBuilder();
    }

    /**
     * Main validation function Checks all needed part according to Xades
     * specification
     *
     * @param file XML file to validate
     * @return string containing messages from validating
     */
    public String validateXml(File file) {

        try {
            setXmlFile(file);
            parseXML();
        } catch (Exception ex) {
            report.append("Nepodarilo sa načítať a rozparsovať XML súbor.\n");
            return report.toString();
        }

        // data envelope validation
        if (validateDataEnvelope()) {
            report.append("Data envelope je v poriadku.\n");
        } else {
            report.append("Data envelope nie je v poriadku!!!\n");
            return report.toString();
        }

        // xml signature validation
        if (validateSignatureMethod()) {
            report.append("Element ds:SignatureMethod je v poriadku\n");
        } else {
            report.append("\nElement ds:SignatureMethod nie je v poriadku!!!\n");
            return report.toString();
        }
        if (validateCanonicalizationMethod()) {
            report.append("Element ds:CanonicalizationMethod je vporiadku.\n");
        } else {
            report.append("Element ds:CanonicalizationMethod nie je vporiadku!!!\n\n");
            return report.toString();
        }
        
        if (validateTransformsMethod()) {
            report.append("Element ds:Transforms je vporiadku.\n");
        } else {
            report.append("\nElement ds:Transforms nie je vporiadku!!!\n\n");
            return report.toString();
        }
        if (validateDigestMethod()) {
            report.append("Element ds:DigestMethod je vporiadku.\n");
        } else {
            report.append("\nElement ds:DigestMethod nie je vporiadku!!!\n");
            return report.toString();
        }

        // core validation
        if (validateSignature()) {
            report.append("Element ds:Signature je vporiadku.\n");
        } else {
            report.append("\nElement ds:Signature nie je vporiadku!!!\n\n");
            return report.toString();
        }
        if (validateSignatureValue()) {
            report.append("Element ds:SignatureValue je vporiadku.\n");
        } else {
            report.append("\nElement ds:SignatureValue je vporiadku!!!\n\n");
            return report.toString();
        }
        
        if (validateSignedInfo()) {
            report.append("Referencie v ds:SignedInfo sú vporiadku.\n");
        } else {
            report.append("\nReferencie v ds:SignedInfo sú vporiadku!!!\n\n");
            return report.toString();
        }
        
        if (validateKeyInfo()) {
            report.append("Element ds:KeyInfo je vporiadku.\n");
        } else {
            report.append("\nElement ds:KeyInfo nie je vporiadku!!!\n\n");
            return report.toString();
        }
        
        if (validateSignatureProperties()) {
            report.append("Element ds:SignatureProperties je vporiadku.\n");
        } else {
            report.append("\nElement ds:SignatureProperties nie je vporiadku.\n\n");
            return report.toString();
        }
        if (validateManifests()) {
            report.append("Element ds:Manifest je vporiadku.\n");
        } else {
            report.append("Element ds:Manifest nie je vporiadku.\n\n");
            return report.toString();
        }

       

        return report.toString();
    }

    private void setXmlFile(File xmlFile) {
        this.xmlFile = xmlFile;
    }

    private void parseXML() throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        doc = db.parse(xmlFile);
    }

    /* =======================   Various validation methods    ======================= */
    private boolean validateDataEnvelope() {
        // root element must be DataEnvelope
        if (doc.getElementsByTagName("xzep:DataEnvelope").item(0) == null) {
            return false;
        }

        // in root element must be defined those NS
        Attr xzep = doc.getDocumentElement().getAttributeNode("xmlns:xzep");
        if (xzep == null) {
            return false;
        }
        String xzepNsValue = xzep.getValue();

        Attr ds = doc.getDocumentElement().getAttributeNode("xmlns:ds");
        if (ds == null) {
            return false;
        }
        String dsNsValuse = ds.getValue();

        return xzepNsValue.equals(xzepNamespace)
                && dsNsValuse.equals(dsNamespace);
    }

    private boolean validateSignatureMethod() {
        Element signature = (Element) doc.getDocumentElement().getElementsByTagName("ds:Signature").item(0);
        if (signature == null) {
            return false;
        }
        Element signatureMethod = (Element) signature.getElementsByTagName("ds:SignatureMethod").item(0);
        if (signatureMethod == null) {
            return false;
        }
        Attr algorithm = signatureMethod.getAttributeNode("Algorithm");
        if (algorithm == null) {
            return false;
        }

        Boolean found = false;
        for (String supportedAlg : supportedSingatureAlgorithms) {
            if (supportedAlg.equals(algorithm.getValue())) {
                found = true;
            }
        }

        if (found == false) {
            return false;
        }

        // all is ok
        return true;
    }

    private boolean validateCanonicalizationMethod() {
        Element signature = (Element) doc.getDocumentElement().getElementsByTagName("ds:Signature").item(0);
        if (signature == null) {
            return false;
        }
        Element canonicalizationMethod = (Element) signature.getElementsByTagName("ds:CanonicalizationMethod").item(0);
        if (canonicalizationMethod == null) {
            return false;
        }
        Attr algorithm = canonicalizationMethod.getAttributeNode("Algorithm");
        if (algorithm == null) {
            return false;
        }

        Boolean found = false;
        for (String supportedAlg : supportedCanonicalizationAlgorithms) {
            if (supportedAlg.equals(algorithm.getValue())) {
                found = true;
            }
        }

        if (found == false) {
            return false;
        }

        // all is ok
        return true;
    }

    private boolean validateTransformsMethod() {
        Element signature = (Element) doc.getDocumentElement().getElementsByTagName("ds:Signature").item(0);
        if (signature == null) {
            return false;
        }

        NodeList referencesMethod = signature.getElementsByTagName("ds:Reference");
        for (int s = 0; s < referencesMethod.getLength(); s++) {
            Element refNode = (Element) referencesMethod.item(s);
            if (refNode.getNodeType() == Node.ELEMENT_NODE) {
                Element transfsElement = (Element) refNode.getElementsByTagName("ds:Transforms").item(0);
                if (transfsElement == null) {
                    return false;
                }
                Element transfElement = (Element) (transfsElement).getElementsByTagName("ds:Transform").item(0);
                if (transfElement == null) {
                    return false;
                }
                Attr algorithm = transfElement.getAttributeNode("Algorithm");

                Boolean found = false;
                for (String supportedAlg : supportedTransformAlgorithms) {
                    if (supportedAlg.equals(algorithm.getValue())) {
                        found = true;
                    }
                }

                if (found == false) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean validateDigestMethod() {
        Element signature = (Element) doc.getDocumentElement().getElementsByTagName("ds:Signature").item(0);
        if (signature == null) {
            return false;
        }
        NodeList referencesMethod = signature.getElementsByTagName("ds:Reference");
        for (int s = 0; s < referencesMethod.getLength(); s++) {
            Node refNode = referencesMethod.item(s);
            if (refNode.getNodeType() == Node.ELEMENT_NODE) {
                Element digestElement = (Element) ((Element) refNode).getElementsByTagName("ds:DigestMethod").item(0);
                if (digestElement == null) {
                    return false;
                }
                Attr algorithm = digestElement.getAttributeNode("Algorithm");

                Boolean found = false;
                for (String supportedAlg : supportedDigestAlgorithms) {
                    if (supportedAlg.equals(algorithm.getValue())) {
                        found = true;
                    }
                }

                if (found == false) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean validateSignature() {
        Element signature = (Element) doc.getDocumentElement().getElementsByTagName("ds:Signature").item(0);
        if (signature == null) {
            return false;
        }
        Attr id = signature.getAttributeNode("Id");
        if (id == null) {
            return false;
        }
        
        Attr namespace = signature.getAttributeNode("xmlns:ds");
        if (namespace == null) {
            return false;
        }
        return true;
    }

    private boolean validateSignatureValue() {
        Element signature = (Element) doc.getDocumentElement().getElementsByTagName("ds:Signature").item(0);
        if (signature == null) {
            return false;
        }
        Element signatureValue = (Element) signature.getElementsByTagName("ds:SignatureValue").item(0);
        if (signatureValue == null) {
            return false;
        }
        Attr attribute = signatureValue.getAttributeNode("Id");
        if (attribute == null) {
            return false;
        }
        return true;
    }

    private boolean validateSignedInfo() {
        Element signature = (Element) doc.getDocumentElement().getElementsByTagName("ds:Signature").item(0);
        if (signature == null) {
            return false;
        }
        Element signedInfo = (Element) signature.getElementsByTagName("ds:SignedInfo").item(0);
        if (signedInfo == null) {
            return false;
        }

        NodeList references = signedInfo.getElementsByTagName("ds:Reference");
        for (int i = 0; i < references.getLength(); i++) {
            Element cur_elem = ((Element) references.item(i));
            if (cur_elem.getNodeName().equals("ds:Reference")) {
                Attr id = cur_elem.getAttributeNode("Id");
                if (id == null) {
                    return false;
                }
                Attr type = cur_elem.getAttributeNode("Type");
                if (type == null) {
                    return false;
                }
                Attr uri = cur_elem.getAttributeNode("URI");
                if (uri == null) {
                    return false;
                }
                

    
                if (id.getValue().contains("KeyInfo")) {
                    if(!type.getValue().equals("http://www.w3.org/2000/09/xmldsig#Object")) {   // zep 2.5 str 22
                        return false;
                    }
                    if (!checkFirstReference("ds:KeyInfo", uri.getValue())) {
                        return false;
                    }
                    break;
                } 
                
                if (id.getValue().contains("SignatureProperties")) {
                    if(!type.getValue().equals("http://www.w3.org/2000/09/xmldsig#SignatureProperties")) {   // zep 2.5 str 22
                        return false;
                    }
                    if (!checkFirstReference("ds:SignatureProperties", uri.getValue())) {
                        return false;
                    }
                    break;
                } 
                
                if (id.getValue().contains("SignedProperties")) {
                    if(!type.getValue().equals("http://uri.etsi.org/01903#SignedProperties")) {   // zep 2.5 str 22
                        return false;
                    }
                    if (!checkFirstReference("xades:SignedProperties", uri.getValue())) {
                        return false;
                    }
                    break;
                } 
                // nebola to ziadna z tycho, musi to byt v manifest!!
                if (!checkManifestReference(uri.getValue())) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean validateKeyInfo() {
        Element signature = (Element) doc.getDocumentElement().getElementsByTagName("ds:Signature").item(0);
        if (signature == null) {
            return false;
        }
        Element keyInfo = (Element) signature.getElementsByTagName("ds:KeyInfo").item(0);
        if (keyInfo == null) {
            return false;
        }
        Attr attribute = keyInfo.getAttributeNode("Id");
        if (attribute == null) {
            return false;
        }
        Element x509Data = (Element) keyInfo.getElementsByTagName("ds:X509Data").item(0);
        if (x509Data == null) {
            return false;
        }
        Element x509Certificate = (Element) x509Data.getElementsByTagName("ds:X509Certificate").item(0);
        if (x509Certificate == null) {
            return false;
        }

        // certificate Byte Structure;
        NodeList x509Certificate_nl = x509Certificate.getChildNodes();
        String cert_s = ((Node) x509Certificate_nl.item(0)).getNodeValue(); // value of element
        byte[] cert_b = Base64.decodeBase64(cert_s);

        // IssuerSerial
        Element x509IssuerSerial = (Element) x509Data.getElementsByTagName("ds:X509IssuerSerial").item(0);
        if (x509IssuerSerial == null) {
            return false;
        }
        Element x509IssuerName = (Element) x509IssuerSerial.getElementsByTagName("ds:X509IssuerName").item(0);
        if (x509IssuerName == null) {
            return false;
        }
        Element x509SerialNumber = (Element) x509IssuerSerial.getElementsByTagName("ds:X509SerialNumber").item(0);
        if (x509SerialNumber == null) {
            return false;
        }
       
        NodeList x509IssuerName_nl = x509IssuerName.getChildNodes();
        String issuerName_s = ((Node) x509IssuerName_nl.item(0)).getNodeValue();
        NodeList x509SerialNumber_nl = x509SerialNumber.getChildNodes();
        String serialNumber_s = ((Node) x509SerialNumber_nl.item(0)).getNodeValue();

        //getting SubjectName
        Element x509SubjectName = (Element) x509Data.getElementsByTagName("ds:X509SubjectName").item(0);
        if (x509SubjectName == null) {
            return false;
        }
        NodeList x509SubjectName_nl = x509SubjectName.getChildNodes();
        String subjectName_s = ((Node) x509SubjectName_nl.item(0)).getNodeValue();

        // tu by to bolo treba overit voce certifikatu, ale ako???

        return true;
    }

    private boolean validateSignatureProperties() {
        Element signatureProperties = (Element) findNode("ds:SignatureProperties", true, true);

        //Attribute Id
        Attr algorithm = signatureProperties.getAttributeNode("Id");
        if (algorithm == null) {
            return false;
        }

        //get Signature Id
        Element signature = (Element) doc.getDocumentElement().getElementsByTagName("ds:Signature").item(0);
        if (signature == null) {
            return false;
        }
        Attr sigId = signature.getAttributeNode("Id");
        if (sigId == null) {
            return false;
        }
        String sigId_s = sigId.getValue();  // note this one, you'll need it...

        //must contain 
        Element signatureProperty1 = (Element) signatureProperties.getElementsByTagName("ds:SignatureProperty").item(0);    // first one
        if (signatureProperty1 == null) {
            return false;
        }
        Element signatureVersion = (Element) signatureProperties.getElementsByTagName("xzep:SignatureVersion").item(0);
        if (signatureVersion == null) {
            return false;
        }
        Attr target1 = signatureProperty1.getAttributeNode("Target");
        if (target1 == null) {
            return false;
        }
        String target1_s = target1.getValue();      // Target nastavený na ds:Signature,
        if (target1_s.charAt(0) == '#') {
            if (!target1_s.substring(1).equals(sigId_s)) {
                return false;
            }
        } else {
            return false;
        }

        //must contain aj druhy element
        Element signatureProperty2 = (Element) signatureProperties.getElementsByTagName("ds:SignatureProperty").item(1);    // second one
        if (signatureProperty2 == null) {
            return false;
        }
        Element productInfos = (Element) signatureProperties.getElementsByTagName("xzep:ProductInfos").item(0);
        if (productInfos == null) {
            return false;
        }
        Attr target2 = signatureProperty2.getAttributeNode("Target");
        if (target2 == null) {
            return false;
        }
        String target2_s = target1.getValue();      // Target nastavený na ds:Signature,
        if (target2_s.charAt(0) == '#') {
            if (!target2_s.substring(1).equals(sigId_s)) {
                return false;
            }
        } else {
            return false;
        }

        return true;
    }

    private boolean validateManifests() {
        Element signature = (Element) doc.getDocumentElement().getElementsByTagName("ds:Signature").item(0);
        if (signature == null) {
            return false;
        }
        NodeList objects = signature.getElementsByTagName("ds:Object");

        //manifest validation
        Element object = (Element) objects.item(objects.getLength() - 1);   //last ds:object contains manifests
        if (object == null) {
            return false;
        }
        NodeList manifests = object.getElementsByTagName("ds:Manifest");
        for (int i = 0; i < manifests.getLength(); i++) {
            Element manifest = (Element) manifests.item(i);
            Attr manifest_ref = manifest.getAttributeNode("Id");
            if (manifest_ref == null) {
                return false;
            }
            Element reference = (Element) manifest.getElementsByTagName("ds:Reference").item(0);
            if (reference == null) {
                return false;
            }

            Element transforms = (Element) reference.getElementsByTagName("ds:Transforms").item(0);
            if (transforms == null) {
                return false;
            }

            // transforms
            NodeList transforms_list = transforms.getElementsByTagName("ds:Transform");
            for (int j = 0; j < transforms_list.getLength(); j++) {
                Element transform = (Element) transforms_list.item(j);
                Attr algorithm = transform.getAttributeNode("Algorithm");
                if (algorithm == null) {
                    return false;
                }
                Boolean found = false;
                for (String supportedAlg : supportedTransformAlgorithms) {
                    if (supportedAlg.equals(algorithm.getValue())) {
                        found = true;
                    }
                }

                if (found == false) {
                    return false;
                }

            }

            //DigestMethod
            Element digestMethod = (Element) reference.getElementsByTagName("ds:DigestMethod").item(0);
            if (digestMethod == null) {
                return false;
            }
            Attr algorithm = digestMethod.getAttributeNode("Algorithm");
            if (algorithm == null) {
                return false;
            }

            Boolean found = false;
            for (String supportedAlg : supportedDigestAlgorithms) {
                if (supportedAlg.equals(algorithm.getValue())) {
                    found = true;
                }
            }

            if (found == false) {
                return false;
            }

            // should follow:   validating Type value against  XAdES_ZEP
            //                  every ds:Manifesr just one ref to ds:Object
        }
        return true;
    }

    
    //Reference verifications
    private boolean checkManifestReference(String url) {
        Element signature = (Element) doc.getDocumentElement().getElementsByTagName("ds:Signature").item(0);
        if (signature == null) {
            return false;
        }
        NodeList objects = signature.getElementsByTagName("ds:Object");
        Element object = (Element) objects.item(objects.getLength() - 1);   // last object in ds:signature is manifest
        if (object == null) {
            return false;
        }
        NodeList manifests = object.getElementsByTagName("ds:Manifest");
        for (int i = 0; i < manifests.getLength(); i++) {
            Element manifest = (Element) manifests.item(i);
            Attr manifest_ref = manifest.getAttributeNode("Id");
            if (manifest_ref != null) {
                String manifest_ref_s = manifest_ref.getValue();
                if (url.charAt(0) == '#') {
                    if (url.substring(1).equals(manifest_ref_s)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    private boolean checkFirstReference(String nodename, String url) {
        Node node = findNode(nodename);
        if (node == null) {
            return false;
        }
        Attr noderef = ((Element) node).getAttributeNode("Id");
        if (noderef == null) {
            return false;
        }
        String noderefStr = noderef.getValue();
        if (url.charAt(0) == '#') {
            if (!url.substring(1).equals(noderefStr)) {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

    public Node findNode(String elementName) {
        return findNode((Element) doc.getDocumentElement(), elementName, true, false);
    }

    public Node findNode(String elementName, boolean deep, boolean elementsOnly) {
        return findNode((Element) doc.getDocumentElement(), elementName, deep, elementsOnly);
    }

    public Node findNode(Node root, String elementName, boolean deep, boolean elementsOnly) {
        if (!(root.hasChildNodes())) {
            return null;
        }

        Node matchingNode = null;
        String nodeName = null;
        Node child = null;

        NodeList childNodes = root.getChildNodes();
        int noChildren = childNodes.getLength();
        for (int i = 0; i < noChildren; i++) {
            if (matchingNode == null) {
                child = childNodes.item(i);
                nodeName = child.getNodeName();
                if ((nodeName != null) & (nodeName.equals(elementName))) {
                    return child;
                }
                if (deep) {
                    matchingNode
                            = findNode(child, elementName, deep, elementsOnly);
                }
            } else {
                break;
            }
        }

        if (!elementsOnly) {
            NamedNodeMap childAttrs = root.getAttributes();
            noChildren = childAttrs.getLength();
            for (int i = 0; i < noChildren; i++) {
                if (matchingNode == null) {
                    child = childAttrs.item(i);
                    nodeName = child.getNodeName();
                    if ((nodeName != null) & (nodeName.equals(elementName))) {
                        return child;
                    }
                } else {
                    break;
                }
            }
        }
        return matchingNode;
    }

    /// <summary>
/// Vyžiadanie podpisového certifikátu časovej pečiatky
/// </summary>
/// <param name="tsResponse">response</param>
/// <returns>Vráti dáta podpisového certifikátu časovej pečiatky. V prípade chyby vráti null.</returns>
//    private byte[] getTimeStampSignatureCertificate(byte[] tsResponse) {
//        try {
//            TimeStampResponse tsResp = new TimeStampResponse(tsResponse);
//            TimeStampToken token = new TimeStampToken(new Ditec.Zep.BouncyCastle.Cms.CmsSignedData(tsResp.TimeStampToken.GetEncoded()));
//            Ditec.Zep.BouncyCastle.X509.X509Certificate signerCert = null;
//            Ditec.Zep.BouncyCastle.X509.Store.IX509Store x509Certs = tsResp.TimeStampToken.GetCertificates("Collection");
//            ArrayList certs = new ArrayList(x509Certs.GetMatches(null));
//
//            // nájdenie podpisového certifikátu tokenu v kolekcii
//            for (Ditec.Zep.BouncyCastle.X509.X509Certificate cert : certs) {
//                String cerIssuerName = cert.IssuerDN.ToString(true, new Hashtable());
//                String signerIssuerName = token.SignerID.Issuer.ToString(true, new Hashtable());
//
//                // kontrola issuer name a seriového čísla
//                if (cerIssuerName == signerIssuerName
//                        && cert.SerialNumber.Equals(token.SignerID.SerialNumber)) {
//                    signerCert = cert;
//                    break;
//                }
//            }
//
//            return signerCert.GetEncoded();
//        } catch (Exception ex) {
//
//            return null;
//        }
//    }

}
